# Single Responsibility Principle

class Student:

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def make_homework(self, home_task=None):
        if home_task:
            return f"{self.first_name} did homework!"
        else:
            return "There was no home task"

    def learn_lessons(self, subject=None):
        if subject:
            return f"{self.first_name} {self.last_name} was present at the {subject}!"
        else:
            return f"{self.first_name} {self.last_name} was absent!"


class Teacher:

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def check_homework(self, homeworks=None):
        if homeworks:
            return f"{self.first_name} {self.last_name} checked homeworks!"
        else:
            return "There was no homeworks for check"

    def do_lecturing(self, subj=None):
        if subj:
            return f"{self.first_name} {self.last_name} did lecturing of {subj}!"
        else:
            return "There was no lecture"


s1 = Student('Paul', 'McCartney')
print(s1.make_homework('task 1'))
print(s1.learn_lessons('math'))

s2 = Student('David', 'Backham')
print(s1.make_homework())
print(s1.learn_lessons())

t1 = Teacher('Elon', 'Mask')
print(t1.check_homework())
print(t1.do_lecturing())

t2 = Teacher('Steve', 'Jobs')
print(t1.check_homework('task 1'))
print(t1.do_lecturing('music'))


