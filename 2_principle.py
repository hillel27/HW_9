# Open‐Closed Principle
from abc import abstractmethod


class Student:

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def make_homework(self, home_task=None):
        if home_task:
            return f"{self.first_name} did homework!"
        else:
            return "There was no home task"

    def learn_lessons(self, subject=None):
        if subject:
            return f"{self.first_name} {self.last_name} was present at the {subject}!"
        else:
            return f"{self.first_name} {self.last_name} was absent!"


class Teacher:

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def check_homework(self, homeworks=None):
        if homeworks:
            return f"{self.first_name} {self.last_name} checked homeworks!"
        else:
            return "There was no homeworks for check"

    def do_lecturing(self, subj=None):
        if subj:
            return f"{self.first_name} {self.last_name} did lecturing of {subj}!"
        else:
            return "There was no lecture"


class ElapsedTime:

    @abstractmethod
    def get_elapsed_time(self):
        pass


class ElapsedTimeStudent(ElapsedTime):

    def __init__(self, days):
        self.days = days

    def __str__(self):
        return str(self.get_elapsed_time())

    def get_elapsed_time(self):
        return self.days * 6


class ElapsedTimeTeacher(ElapsedTime):

    def __init__(self, days):
        self.days = days

    def __str__(self):
        return str(self.get_elapsed_time())

    def get_elapsed_time(self):
        return self.days * 8


s1 = Student('Paul', 'McCartney')
print(f'{s1.first_name} {s1.last_name} :')
time_s1 = ElapsedTimeStudent(10)
print(time_s1)

t1 = Teacher('Elon', 'Mask')
print(f'{t1.first_name} {t1.last_name} :')
time_t1 = ElapsedTimeTeacher(10)
print(time_t1)

