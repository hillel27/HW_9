# Dependency Inversion Principle
from abc import abstractmethod


class Human:

    @abstractmethod
    def get_hobbies(self, hobby):
        pass


class Person(Human):

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def get_hobbies(self, hobby):
        return f'Hobby is {hobby}'


class DoingHomework:

    def __init__(self, home_task):
        self.home_task = home_task

    def doing_homework(self):
        if self.home_task:
            return "Homework is done!"
        else:
            return "There was no home task"


class LearnLessons:

    def __init__(self, lesson):
        self.lesson = lesson

    def learn_lessons(self):
        if self.lesson:
            return "Lesson was visiting!"
        else:
            return f"Was absent at the lesson!"


class CheckHomework:

    def __init__(self, homeworks):
        self.homeworks = homeworks

    def checking_hw(self):
        if self.homeworks:
            return f"Homeworks are checked!"
        else:
            return "There was no homeworks for check"


class DoLecturing:

    def __init__(self, lecture):
        self.lecture = lecture

    def do_lecturing(self):
        if self.lecture:
            return f"Did lecturing!"
        else:
            return "There was no lecture."


class Student(Person, DoingHomework, LearnLessons):

    def __init__(self, first_name, last_name, group, home_task, lesson):
        super().__init__(first_name, last_name)
        self.group = group
        self.home_task = home_task
        self.lesson = lesson


class Teacher(Person, CheckHomework, DoLecturing):

    def __init__(self, first_name, last_name, subject, homeworks, lecture):
        super().__init__(first_name, last_name)
        self.subject = subject
        self.homeworks = homeworks
        self.lecture = lecture


s1 = Student('Paul', 'McCartney', 'Group 10', '', 'lesson 1')
print(f'Student {s1.first_name} {s1.last_name} : {s1.doing_homework()}')
print(f'Student {s1.first_name} {s1.last_name} : {s1.learn_lessons()}')
print(f'Student {s1.first_name} {s1.last_name} :', s1.get_hobbies('football'))

t1 = Teacher('Ilon', 'Mask', 'Math', 'test 1', 'lecture 1')
print(f'Teacher {t1.first_name} {t1.last_name} : {t1.checking_hw()}')
print(f'Teacher {t1.first_name} {t1.last_name} : {t1.do_lecturing()}')
print(f'Teacher {t1.first_name} {t1.last_name} :', t1.get_hobbies('skating'))