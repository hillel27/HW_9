# Liskov Substitution Principle

class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


class Student(Person):

    def __init__(self, first_name, last_name, group):
        super().__init__(first_name, last_name)
        self.group = group

    def make_homework(self, home_task=None):
        if home_task:
            return f"{self.first_name} from {self.group} did homework!"
        else:
            return f"There was no home task for {self.group}"

    def learn_lessons(self, subject=None):
        if subject:
            return f"{self.first_name} {self.last_name} was present at the {subject}!"
        else:
            return f"{self.first_name} {self.last_name} was absent!"


class Teacher(Person):

    def __init__(self, first_name, last_name, subject):
        super().__init__(first_name, last_name)
        self.subject = subject

    def check_homework(self, homeworks=None):
        if homeworks:
            return f"{self.first_name} {self.last_name} checked homeworks on {self.subject}!"
        else:
            return "There was no homeworks for check"

    def do_lecturing(self, subj=None):
        if subj:
            return f"{self.first_name} {self.last_name} did lecturing of {subj}!"
        else:
            return "There was no lecture"


s1 = Student('Paul', 'McCartney', 'Group 10')
print(s1.make_homework('task 1'))
print(s1.learn_lessons('math'))

s2 = Student('David', 'Backham', 'Group 11')
print(s1.make_homework())
print(s1.learn_lessons())

t1 = Teacher('Ilon', 'Mask', 'Math')
print(t1.check_homework())
print(t1.do_lecturing())

t2 = Teacher('Steve', 'Jobs', 'Computer Science')
print(t1.check_homework('task 1'))
print(t1.do_lecturing('music'))